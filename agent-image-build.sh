#!/bin/bash
# execute script in this directory
cd "$(dirname ${0})"

# # create a new project for testing
# oc new-project non-root-image-builds

# # create the builds for each agent image
# oc new-build --name non-root-jenkins-agent-base  --binary=true --strategy=docker
# oc new-build --name non-root-jenkins-agent-python --binary=true --strategy=docker

# # build the base jenkins agent
# cp -v Dockerfile.base /tmp/Dockerfile
# oc start-build non-root-jenkins-agent-base --from-file=/tmp/Dockerfile --wait --follow
# rm -f /tmp/Dockerfile

# # build the python jenkins agent
# cp -v Dockerfile.python /tmp/Dockerfile
# oc start-build non-root-jenkins-agent-python --from-file=/tmp/Dockerfile --wait --follow
# rm -f /tmp/Dockerfile

# oc new-app jenkins-ephemeral

exit_usage() {

cat <<EOF

Usage: $0 command

where commans in

    build: build images
    push:  push images to docker hub (ubdems)
    rel:   build and push to docker hub (ubdems)
    run:   run  jenkins agent

todo:
   check jenkins ports and secrets
erm
EOF
exit 1


}

do_login() {
    
    podman login -u ubdems docker.io
   
    rc=$?
}


do_build() {

    podman build -f Dockerfile.base   -t ubdems/jenkins-jnlp-agent-podman:latest   && \
    podman build -f Dockerfile.python -t ubdems/jenkins-jnlp-agent-podman-python:latest

    rc=$?

}


do_push() {
    
    podman push ubdems/jenkins-jnlp-agent-podman:latest docker.io/ubdems/jenkins-jnlp-agent-podman:latest
    podman push ubdems/jenkins-jnlp-agent-podman-python:latest docker.io/ubdems/jenkins-jnlp-agent-podman-python:latest
   
    rc=$?
}

do_release() {

    do_build   && \
    do_push

    rc=$?

}




do_run() {

    # ssh jk66

    

    set -x

JP=1; \
: ${X_JENKINS_AGENT_URL:='http://localhost:31066/computer/g5/jenkins-agent.jnlp'} ; \
: ${X_JENKINS_AGENT_WORKDIR:="/home/jenkins/pipe/local.00"} ; \
: ${X_JENKINS_AGENT_SECRET='870ecdc461eb88140ab901463fd18112a22f17a32f640b1f8d0c65b94f90d79c'} ; \
if [ "$(echo -e "$(uname -r)\n5.13" | sort -V | head -n1)" = '5.13' ]; then \
PD_OVRLAY=1; \
podman pull docker.io/ubdems/jenkins-jnlp-agent-podman-python:latest ; \        
podman  --log-level=debug run --rm --init \
        --network=host \
        --cap-add SYS_ADMIN \
        --cap-add all \
        --security-opt seccomp=unconfined \
        -v ~/pipe:/home/podman/pipe:Z \
        -e JENKINS_AGENT_URL=${X_JENKINS_AGENT_URL} \
        -e JENKINS_AGENT_WORKDIR=${X_JENKINS_AGENT_WORKDIR} \
        -e JENKINS_AGENT_SECRET=${X_JENKINS_AGENT_SECRET} \
        ubdems/jenkins-jnlp-agent-podman-python:latest ; \
else \
PD_OVRLAY=0; \
podman pull docker.io/ubdems/jenkins-jnlp-agent-podman-python:latest ; \
mkdir -p ~/.local/podman/containers; \
mkdir -p ~/pipe/local.00; \
podman  --log-level=debug run --rm --init \
        --network=host \
        --cap-add=sys_admin,mknod,chown \
        --security-opt label=disable \
        --device=/dev/fuse \
        -v ~/.local/podman/containers:/home/podman/.local/share/containers \
        -v ~/pipe:/home/podman/pipe:Z \
        -e JENKINS_AGENT_URL=${X_JENKINS_AGENT_URL} \
        -e JENKINS_AGENT_WORKDIR=${X_JENKINS_AGENT_WORKDIR} \
        -e JENKINS_AGENT_SECRET=${X_JENKINS_AGENT_SECRET} \
        ubdems/jenkins-jnlp-agent-podman-python:latest ;\
fi        


# podman --log-level=debug run --security-opt label=disable --user podman  --device /dev/fuse -ti --entrypoint /bin/bash docker.io/ubdems/jenkins-jnlp-agent-podman:latest -c "podman info" 2>&1 | sed -e 's/^time=[^ ]* //' | tee /tmp/pinp-podman-agent-$(uname -r)-$(date -Isec).log | less -SRX
# podman --log-level=debug run --security-opt label=disable --user podman  --device /dev/fuse -ti --entrypoint /bin/bash quay.io/podman/stable                             -c "podman info" 2>&1 | sed -e 's/^time=[^ ]* //' | tee /tmp/pinp-podman-basic-$(uname -r)-$(date -Isec).log | less -SRX

#        --entrypoint podman info \
    
#        --entrypoint /bin/bash \
#        --rm -ti \

#        --storage-driver overlay \
#        --storage-opt overlay.mount_program=/usr/bin/fuse-overlayfs \

#        --log-level debug \       
        

    set +x

    rc=$?
    
}

rc=0
case "$1" in
    login)   do_login ;;
    push)    do_push ;;
    build)   do_build ;;
    rel)     do_release ;;
    run)     do_run ;;
    *)       exit_usage ;;
esac
exit $rc
